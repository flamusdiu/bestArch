If you want to contribute to this repo and make a new section, run `markdown-toc -i README.md` to update the table of contents.

To install `markdown-toc` run `sudo npm install -g markdown-toc`.
