# bestArch

Configurations and todos to make your Arch Linux the best Arch Linux

<!-- toc -->

- [System](#system)
  * [Make an encrypted install](#make-an-encrypted-install)
  * [Use systemd-boot](#use-systemd-boot)
  * [Microcode updates](#microcode-updates)
    + [AMD](#amd)
    + [Intel](#intel)
  * [Compress initramfs with lz4](#compress-initramfs-with-lz4)
  * [Limit journald log size](#limit-journald-log-size)
  * [Disable core dumps](#disable-core-dumps)
  * [Change IO Scheduler](#change-io-scheduler)
  * [Change CPU governor](#change-cpu-governor)
  * [Setting up Plymouth](#setting-up-plymouth)
  * [Create a swap file](#create-a-swap-file)
    + [Removing the swap file if not necessary/wanted anymore](#removing-the-swap-file-if-not-necessarywanted-anymore)
    + [Alternative route](#alternative-route)
  * [Enable Hibernation](#enable-hibernation)
- [Package Management](#package-management)
  * [Switch to better mirrors](#switch-to-better-mirrors)
  * [Enable colors in pacman](#enable-colors-in-pacman)
  * [Enable parallel compilation and compression](#enable-parallel-compilation-and-compression)
  * [Install flatpaks in user directory with GNOME Software by default](#install-flatpaks-in-user-directory-with-gnome-software-by-default)
  * [Use another computer as repository](#use-another-computer-as-repository)
- [Networking](#networking)
  * [Network printing](#network-printing)
  * [DNSCrypt](#dnscrypt)
    + [Install](#install)
    + [Configure](#configure)
- [Graphics](#graphics)
  * [Copy monitor layout from user to GDM](#copy-monitor-layout-from-user-to-gdm)
  * [NVIDIA driver DRM kernel mode setting](#nvidia-driver-drm-kernel-mode-setting)
  * [Fix screen tearing with NVIDIA GPUs](#fix-screen-tearing-with-nvidia-gpus)
- [Multimedia](#multimedia)
  * [Fix bluetooth audio](#fix-bluetooth-audio)
  * [MPV hardware decoding (NVIDIA VDPAU)](#mpv-hardware-decoding-nvidia-vdpau)
- [Miscellaneous](#miscellaneous)
  * [Setup libvirt](#setup-libvirt)

<!-- tocstop -->

# System

## Make an encrypted install

**NOTE**: In this tutorial I will assume you're working with a single drive indicated as `${DISK}`. I will refer to `${DISK}1`, `${DISK}2` etc as partition 1, partition 2 and so on. If you want to copy paste these commands, you can if you set the `DISK` variable in your shell to something like `/dev/sda`. If you have an nvme drive you CANNOT do this, since an nvme drive is indicated as `/dev/nvme0n1` while partitions are indicated as `/dev/nvme0n1p1`, `/dev/nvme0n1p2` etc. I will also assume you want to install using UEFI/systemd-boot.

**Raw partition table overview**

| Partition  | Format | Mount point             |
| ---------- | ------ | ----------------------- |
| `${DISK}1` | FAT32  | `/boot`                 |
| `${DISK}2` | LUKS   | *No direct mount point* |

Create a **GPT** partition table with `parted` as follows:

```bash
parted ${DISK}
mklabel gpt
q
```

Use `gdisk` to create the partitions. **NOTE**: I will write `# press enter` when I need you to just press the *enter* key without writing anything

```bash
gdisk ${DISK}
n
# press enter
# press enter
+400M
ef00
n
# press enter
# press enter
# press enter
8e00
w
y
```

Complete the setup as follows

```bash
# Format the first partition to *FAT32*.
mkfs.vfat -F32 ${DISK}1
# Format the second partition as a LUKS encrypted container
cryptsetup luksFormat --type luks2 ${DISK}2
# Open the just created container
cryptsetup open ${DISK}2 cryptlvm
# Create a physical volume on top of the opened LUKS container
pvcreate /dev/mapper/cryptlvm
# Create a volume group with a name you want (I usually choose MasterVolumeGroup)
# and add the previously created physical volume to it
vgcreate MasterVolumeGroup /dev/mapper/cryptlvm
# Create the logical volumes on the volume group for the root and home partitions
lvcreate -L 40G MasterVolumeGroup -n root
lvcreate -l 100%FREE MasterVolumeGroup -n home
# Format the newly created logical volumes
mkfs.ext4 /dev/MasterVolumeGroup/root
mkfs.ext4 /dev/MasterVolumeGroup/home
# Mount the filesystem
mount /dev/MasterVolumeGroup/root /mnt
mkdir /mnt/home
mkdir /mnt/boot
mount /dev/MasterVolumeGroup/home /mnt/home
mount ${DISK}1 /mnt/boot
```

Before proceeding with the installation, you have to edit `/etc/mkinitcpio.conf` as follows:

- Add **`keyboard keymap`** after `base udev autodetect` in the **`HOOKS`** array
- Add **`encrypt lvm2`** after `consolefont modconf block` in the **`HOOKS`** array

Proceed with the remaining part of the installation as normal. Refer to [Use systemd-boot](#use-systemd-boot) to install the systemd-boot bootloader.

## Use systemd-boot

[Arch Wiki reference](https://wiki.archlinux.org/index.php/Systemd-boot#Installation)

Requires you to be able to boot in UEFI mode (not MBR).

You need to have a `/boot` partition formatted in FAT32 (usually I make it 400 MBs, even if it's a little too much).

Assuming you have all your file systems mounted to their proper locations AND that you are already chroot-ed in your installed system.

```bash
sudo bootctl --path=/boot install
```

Create `/boot/loader/entries/arch.conf` like follows:

```
title		Arch Linux
linux		/vmlinuz-linux
# uncomment this in case you want to install intel microcode
# initrd		/intel-ucode.img
# initrd		/amd-ucode.img
initrd		/initramfs-linux.img
options		root=UUID=ROOT_PARTITION_UUID rw quiet # nvidia-drm.modeset=1 # uncomment this if/when you want to enable nvidia drm kernel mode setting
# use the following options row in alternative to this ^^^ one if you have an encrypted setup
# options cryptdevice=UUID=ENCRYPTED_PARTITION_UUID:cryptlvm root=/dev/MasterVolumeGroup/root rw quiet
```

Where `ROOT_PARTITION_UUID` or `ENCRYPTED_PARTITION_UUID` can be obtained from the command `lsblk -f` (use the UUID of the partition mounted as `/`).

You may want to edit `/boot/loader/loader.conf` to set a timeout (format: `timeout TIME_IN_SECONDS`) and to add a line that says `default arch-*`.

Install [`systemd-boot-pacman-hook`](https://aur.archlinux.org/packages/systemd-boot-pacman-hook/)<sup>AUR</sup> to automatically update systemd-boot.

## Microcode updates

[Arch wiki reference](https://wiki.archlinux.org/index.php/Microcode#Enabling_Intel_microcode_updates)

### AMD

```bash
sudo pacman -S amd-ucode
```

Edit `/boot/loader/entries/arch.conf` so that the first `initrd` line is the following:

```
initrd        /amd-ucode.img
```

### Intel

```bash
sudo pacman -S intel-ucode
```

Edit `/boot/loader/entries/arch.conf` so that the first `initrd` line is the following:

```
initrd        /intel-ucode.img
```

## Compress initramfs with lz4

Make sure `lz4` is installed.

Edit `/etc/mkinitcpio.conf`:

- Add `lz4 lz4_compress` to the `MODULES` list (delimited by `()`)
- Uncomment or add the line saying `COMPRESSION="lz4"`
- Add a line saying `COMPRESSION_OPTIONS="-9"`
- Add `shutdown` to the `HOOKS` list (delimited by `()`)

Run `sudo mkinitcpio -p linux` to apply the mkinitcpio.conf changes.

## Limit journald log size

Edit `/etc/systemd/journald.conf`:

- Uncomment `SystemMaxUse=` and append `200M` (or any size you like).

## Disable core dumps

To improve performance and save disk space.

Edit `/etc/systemd/coredump.conf`, under `[Coredump]` uncomment `Storage=external` and replace it with `Storage=none`. Then run `sudo systemctl daemon-reload`. This alone disables the saving of coredumps but they are still in memory.

If you want to disable core dumps completely add `* hard core 0` to `/etc/security/limits.conf`.

## Change IO Scheduler

## Change CPU governor

[Arch Wiki reference](https://wiki.archlinux.org/index.php/CPU_frequency_scaling)

```bash
sudo pacman -S cpupower
```

To change the governor for the current session run `sudo cpupower frequency-set -g performance`.

To change the governor on boot create a systemd service.

Create `/etc/systemd/system/cpupower.service`:

```
[Unit]
Description=Set CPU governor to performance

[Service]
Type=oneshot
ExecStart=/usr/bin/cpupower -c all frequency-set -g performance

[Install]
WantedBy=multi-user.target
```

Finally run `sudo systemctl enable cpupower.service`.

*NB: the default governor is powersave and you may want to leave it as it is.*

Create `/etc/udev/rules.d/50-scaling-governor.rules` as follows:

```
SUBSYSTEM=="module", ACTION=="add", KERNEL=="acpi_cpufreq", RUN+=" /bin/sh -c ' echo performance > /sys/devices/system/cpu/cpufreq/policy0/scaling_governor ' "
```

## Setting up Plymouth

*NOTE: this setup implies that you use yay (AUR helper), gdm (display manager), and the default arch kernel.*

```bash
yay -S plymouth-git gdm-plymouth
```

Edit `/etc/mkinitcpio.conf`:

- In `HOOKS` after `base udev` insert `plymouth`
- If you're using encryption, in `HOOKS` replace `encrypt` with `plymouth-encrypt`
- In `MODULES` insert your GPU driver module name as first item
  - For Intel GPUs: `i915`
  - For AMD GPUs: `radeon` *(note: this is untested)*
  - For NVIDIA GPUs: `nvidia` *(note: this is untested)* 
  - For KVM/qemu VMs: `qxl`

Edit `/boot/loader/entries/arch-linux.conf`: add these arguments in the kernel options (append to the `options` section): `quiet splash loglevel=3 rd.udev.log_priority=3 vt.global_cursor_default=1`

```bash
sudo systemctl disable gdm
sudo systemctl enable gdm-plymouth
sudo mkinitcpio -p linux
```

## Create a swap file

[Arch Wiki reference](https://wiki.archlinux.org/index.php/Swap#Swap_file)

A form of swap is required to enable hibernation.

In this example we will allocate a 8G swap file.

```bash
sudo dd if=/dev/zero of=/home/swapfile bs=1M count=8192
sudo chmod 600 /home/swapfile
sudo mkswap /home/swapfile
sudo swapon /home/swapfile # this enables the swap file for the current session
```

Edit `/etc/fstab` adding the following line:

```
/home/swapfile none swap defaults 0 0
```

### Removing the swap file if not necessary/wanted anymore

```
sudo swapoff -a
```

Edit `/etc/fstab` and remove the swapfile entry, and finally:

```
sudo rm -f /home/swapfile
```

### Alternative route

Use systemd-swap for automated and dynamic swapfile allocation and use. Consult [the GitHub project page](https://github.com/Nefelim4ag/systemd-swap) for more info.

## Enable Hibernation

[Arch Wiki reference](https://wiki.archlinux.org/index.php/Power_management/Suspend_and_hibernate#Hibernation_into_swap_file)

# Package Management

## Switch to better mirrors

[Arch Wiki reference](https://wiki.archlinux.org/index.php/Reflector)

```bash
sudo pacman -S reflector
sudo reflector --latest 200 --protocol http --protocol https --sort rate --save /etc/pacman.d/mirrorlist
```

## Enable colors in pacman

Edit `/etc/pacman.conf` and uncomment the row saying `Color`

## Enable parallel compilation and compression

Edit `/etc/makepkg.conf`:

- Add the following row (replace 7 with CPU threads-1): `MAKEFLAGS="-j7"`
- Edit the row saying `COMPRESSXZ=(xz -c -z -)` to `COMPRESSXZ=(xz -c -z - --threads=0)`
- `sudo pacman -S pigz` and edit the row saying `COMPRESSGZ=(gzip -c -f -n)` to `COMPRESSGZ=(pigz -c -f -n)`

## Install flatpaks in user directory with GNOME Software by default

```bash
gsettings set org.gnome.software install-bundles-system-wide false
```

## Use another computer as repository

[Arch wiki reference](https://wiki.archlinux.org/index.php/Pacman/Tips_and_tricks#Network_shared_pacman_cache)

> If you happen to run several Arch boxes on your LAN, you can share packages so that you can greatly decrease your download times

On the source computer install `darkhttpd` and run:

```bash
sudo ln -s /var/lib/pacman/sync/*.db /var/cache/pacman/pkg
sudo -u http darkhttpd /var/cache/pacman/pkg --no-server-id
```

On the destination computer edit `/var/pacman.d/mirrorlist` and insert `Server = http://<source_machine_ip>:8080` as the first line of the file, replacing `<source_machine_ip>` with the ip of your source computer (to get a computer's ip run `ip -c a`).

You're all set up. Once you're done, close darkhttpd on the source computer and comment/delete the line you just added on the destination computer.

This could be useful if you have multiple machines on the same LAN, maybe sharing a slow connection, or even if you want to make an arch installation without an internet connection.

# Networking

## Network printing

- [Arch wiki reference (CUPS#Network)](https://wiki.archlinux.org/index.php/CUPS#Network)
- [Arch wiki reference (Avahi#Hostname_Resolution)](https://wiki.archlinux.org/index.php/Avahi#Hostname_resolution)

For good measure when using CUPS, make sure you have installed all cups packages I recommend below. They are all included in the *commonpackages* file in this repo.

```
foomatic-db foomatic-db-engine foomatic-db-gutenprint-ppds foomatic-db-nonfree foomatic-db-nonfree-ppds foomatic-db-ppds gutenprint cups cups-filters cups-pdf cups-pk-helper
```

Also make sure that your user is part of these 3 groups:

- `sys`
- `lp`
- `cups`

In one command: `sudo usermod -aG sys,lp,cups $USERNAME`.

Install `nss-mdns`.

Edit `/etc/nsswitch.conf` adding `mdns_minimal [NOTFOUND=return]` right before `resolve [!UNAVAIL=return]`.

```
hosts: ... mdns_minimal [NOTFOUND=return] resolve [!UNAVAIL=return] dns ...
#          ------------------------------
```

Do a `sudo systemctl start avahi-daemon.service` (or restart if necessary). Also do `sudo systemctl restart org.cups.cupsd.service` and `sudo systemctl restart cups-browsed.service` for good measure.

If you have the correct drivers for the network printer, it should now be usable. Also, if GUI printer managers fail, try the CUPS web interface at [`localhost:631`](http://localhost:631).

## DNSCrypt

[Arch Wiki reference](https://wiki.archlinux.org/index.php/DNSCrypt)

Encrypt your DNS traffic so your ISP can't spy on you. Use `pdnsd` as a proxy and cache for it.

### Install

```bash
sudo pacman -S dnscrypt-proxy pdnsd
```

### Configure

Edit `/etc/dnscrypt-proxy/dnscrypt-proxy.toml`:

- Uncomment the `server_names` list (line 30) and change it as follows: `server_names = ['de.dnsmaschine.net', 'trashvpn']` (see *Note* below)
- Change the `listen_address` list (line 36) to an empty list: `listen_address = []` (we're using systemd socket, this avoids port conflicts)

*Note: you can find more "Resolvers" in `/usr/share/dnscrypt-proxy/dnscrypt-resolvers.csv` or [here](https://github.com/dyne/dnscrypt-proxy/blob/master/dnscrypt-resolvers.csv)*

Edit `/usr/lib/systemd/system/dnscrypt-proxy.service` to include the following:

```
[Service]
DynamicUser=yes
```

Edit `/usr/lib/systemd/system/dnscrypt-proxy.socket` to change the port dnscrypt runs on. Here is a snippet:

```
[Socket]
ListenStream=127.0.0.1:53000
ListenDatagram=127.0.0.1:53000
```

Create `/etc/pdnsd.conf` like so:

```
global {
	perm_cache=1024;
	cache_dir="/var/cache/pdnsd";
#	pid_file = /var/run/pdnsd.pid;
	run_as="pdnsd";
	server_ip = 127.0.0.1;  # Use eth0 here if you want to allow other
				# machines on your network to query pdnsd.
	status_ctl = on;
#	paranoid=on;       # This option reduces the chance of cache poisoning
	                   # but may make pdnsd less efficient, unfortunately.
	query_method=udp_tcp;
	min_ttl=15m;       # Retain cached entries at least 15 minutes.
	max_ttl=1w;        # One week.
	timeout=10;        # Global timeout option (10 seconds).
	neg_domain_pol=on;
	udpbufsize=1024;   # Upper limit on the size of UDP messages.
}

server {
    label = "dnscrypt-proxy";
    ip = 127.0.0.1;
    port = 53000;
    timeout = 4;
    proxy_only = on;
}

source {
	owner=localhost;
#	serve_aliases=on;
	file="/etc/hosts";
}

rr {
	name=localhost;
	reverse=on;
	a=127.0.0.1;
	owner=localhost;
	soa=localhost,root.localhost,42,86400,900,86400,86400;
}
```

Reload systemd daemons, enable and start services:

```bash
sudo systemctl daemon-reload
sudo systemctl enable dnscrypt-proxy.service pdnsd.service
sudo systemctl start dnscrypt-proxy.service pdnsd.service
```

Edit your NetworkManager configuration to point to the following IPs for respectively IPv4 and IPv6 DNSes:

```
127.0.0.1
::1
```

# Graphics

## Copy monitor layout from user to GDM

GDM doesn't know how you configure your monitors. It just keep its default configuration and most of the time it's not the same of how you have them configured in your session.

To copy your user's monitors configuration over to GDM, use these commands:

```bash
sudo cp $HOME/.config/monitors.xml /var/lib/gdm/.config/
sudo chown gdm:gdm /var/lib/gdm/.config/monitors.xml
```

## NVIDIA driver DRM kernel mode setting

[Arch Wiki reference](https://wiki.archlinux.org/index.php/NVIDIA#DRM_kernel_mode_setting)

Edit `/boot/loader/entries/arch.conf` appending `nvidia-drm.modeset=1` to the `options` row.

Edit `/etc/mkinitcpio.conf` prepending to the `MODULES` list (delimited by `()`) the following: `nvidia nvidia_modeset nvidia_uvm nvidia_drm`.

Run `sudo mkinitcpio -p linux` to apply the mkinitcpio.conf changes.

Add a pacman hook to rebuild initramfs after an NVIDIA driver upgrade, create `/etc/pacman.d/hooks/nvidia.hook`:

```
[Trigger]
Operation=Install
Operation=Upgrade
Operation=Remove
Type=Package
Target=nvidia

[Action]
Depends=mkinitcpio
When=PostTransaction
Exec=/usr/bin/mkinitcpio -P
```

*NB: Make sure the Target package set in this hook is the one you have installed (`nvidia`, `nvidia-lts` or some other different or legacy driver package name).*

Edit `/etc/gdm/custom.conf` uncommenting the row that says `WaylandEnable=false` (enabling DRM kernel mode setting usually improves performance but enables NVIDIA Wayland support for GNOME, but currently NVIDIA Wayland performance is terrible and makes for an unusable experience. While this option is not mandatory, it's highly recommended).

## Fix screen tearing with NVIDIA GPUs

First you need a base xorg config file to work with. To obtain it, run `nvidia-xconfig -o ./nvidia-xorg.conf`.

Open it up and strip out everything but the **Device** and **Screen** sections.

In the **Device** section you may want to add a line like this: `BoardName "<GPU Model>"`. For example, with my GTX 960 I have `BoardName  "GeForce GTX 960"`.

In the **Screen** section, delete everything but the lines saying **Identifier**, **Device** and **Monitor**. In the same section add the following lines:

```
Option "AllowIndirectGLXProtocol" "off"
Option "TripleBuffer" "on"
```

Finally, again in the **Screen** section, add a line that's a little bit complex, so we can build it up step by step.

You have to write a comma separated list of the video outputs that you use, corresponding resolutions, refresh rates and offsets. You can make this process easier by using xrandr.

In a terminal, type in `xrandr | grep " connected"`. You will see something like this:

```
HDMI-0 connected 1920x1080+2560+360 (normal left inverted right x axis y axis) 521mm x 293mm
DP-2 connected primary 2560x1440+0+0 (normal left inverted right x axis y axis) 597mm x 336mm
```

The initial part of the line is always `Option "metamodes" `.

Following, inside quotes, write `"<output name>: <resolution width>x<resolution height>_<refresh_rate> +<offset x>+<offset y> {ForceCompositionPipeline=On, ForceFullCompositionPipeline=On}, <repeat for all outputs>"`.

For reference, here's my line: `Option "metamodes" "DP-2: 2560x1440_60 +0+0 {ForceCompositionPipeline=On, ForceFullCompositionPipeline=On}, HDMI-0: 1920x1080_60 +2560+360 {ForceCompositionPipeline=On, ForceFullCompositionPipeline=On}"`.

Again, for reference, here's my full file:

```
Section "Device"
        Identifier "Device0"
        Driver     "nvidia"
        VendorName "NVIDIA Corporation"
        BoardName  "GeForce GTX 960"
EndSection

Section "Screen"
    Identifier     "Screen0"
    Device         "Device0"
    Monitor        "Monitor0"
    Option         "AllowIndirectGLXProtocol" "off"
    Option         "TripleBuffer" "on"
    Option         "metamodes" "DP-2: 2560x1440_60 +0+0 {ForceCompositionPipeline=On, ForceFullCompositionPipeline=On}, HDMI-0: 1920x1080_60 +2560+360 {ForceCompositionPipeline=On, ForceFullCompositionPipeline=On}"
EndSection
```

Once you have a file that looks like this, rename it to 20-nvidia.conf and move or copy it to `/etc/X11/xorg.conf.d/`.

# Multimedia

## Fix bluetooth audio

[Arch Wiki reference](https://wiki.archlinux.org/index.php/Bluetooth_headset)

Prevent GDM from spawning pulseaudio: edit `/var/lib/gdm/.config/pulse/client.conf` like so:

```
autospawn = no
daemon-binary = /bin/true
```

Finally run:

```bash
sudo -ugdm mkdir -p /var/lib/gdm/.config/systemd/user
sudo -ugdm ln -s /dev/null /var/lib/gdm/.config/systemd/user/pulseaudio.socket
```

## MPV hardware decoding (NVIDIA VDPAU)

Install `nvidia-utils` (or similar package depending on your nvidia driver) and `libva-vdpau-driver`.

Create or edit `.config/mpv/mpv.conf`:

```
vo=vdpau
profile=opengl-hq
hwdec=vdpau
hwdec-codecs=all
scale=ewa_lanczossharp
cscale=ewa_lanczossharp
interpolation
tscale=oversample
```

# Miscellaneous

## Setup libvirt

```bash
sudo pacman -S libvirt ebtables dnsmasq bridge-utils virt-manager
sudo gpasswd -a $USERNAME libvirt
sudo gpasswd -a $USERNAME kvm
sudo systemctl enable libvirtd
sudo systemctl start libvirtd
```

Make sure to relogin after following the steps above. To create a network:

- Open virt-manager
- Click on *QEMU/KVM*
- Click *Edit > Connection Details* in the menu
- Click the *Virtual Networks* tab
- Click the `+` (plus sign) button in the bottom left corner of the newly opened window
- Name it whatever
- Select *NAT* as Mode
- Leave everything else as it is
- Click finish
- To start the network, select it in the sidebar and press the ▶️ (play icon) button
- To stop the network, press the icon to its left with the 🛑 (stop street sign icon) button (note: the icons could be different depending on the theme)
- To start the network on boot, select it in the sidebar and toggle the checkbox that says *Autostart: On Boot*
