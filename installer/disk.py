import subprocess
import json
import os

EXIT_ERRORS = {
    '?': '',
    'SHELL': 'Error: Shell error. Something\'s wrong, please try again.'
}

BYTE_DIVIDER = 1024

def failure_exit(errcode):
    print(EXIT_ERRORS[errcode])
    exit(1)

def run_cmd(cmd, errcode, output=False):
    try:
        process = subprocess.run(cmd, shell=True, check=True, capture_output=output)
    except subprocess.CalledProcessError as err:
        failure_exit(errcode)
    if output:
        return process.stdout.decode()

class Partition:

    def __init__(self, name, size, uuid=None):
        self.name = name
        self.size = size
        self.uuid = uuid

    def __repr__(self):
        size_measures = [
            'Bytes',
            'KB',
            'MB',
            'GB',
            'TB'
        ]
        size_index = 0
        size = self.size
        while size > BYTE_DIVIDER and size_index+1 < len(size_measures):
            size /= BYTE_DIVIDER
            size_index += 1
        return '{0}: {1} {2}'.format(
            self.name,
            '%.2f' % size,
            size_measures[size_index]
        )

class Disk(Partition):

    def __init__(self, name, size):
        super(Disk, self).__init__(name, size)
        self.partitions = []

    def __repr__(self):
        out = super(Disk, self).__repr__()
        for p in self.partitions:
            out += '\n  - ' + p.__repr__()
        return out

def find_partitions():
    cmdout = run_cmd('lsblk -plbJ -o +UUID', 'SHELL', True)
    out = json.loads(cmdout)['blockdevices']
    disks = []
    parts = []
    for d in out:
        if d['type'] == 'disk':
            disks.append(d)
        elif d['type'] == 'part':
            parts.append(d)
    disk_objs = []
    for d in disks:
        disk_objs.append(Disk(d['name'], d['size']))
    for d in disk_objs:
        for p in parts:
            if d.name in p['name']:
                d.partitions.append(Partition(p['name'], p['size'], p['uuid']))
    return disk_objs

def choose_disk(disks):
    choice = -1
    while choice < 0 or choice >= len(disks):
        print('\n\nChoose a disk for installation\n\n')
        for i, d in enumerate(disks):
            print('### {0}'.format(i))
            print(d)
        try:
            choice = int(input('\n>>> '))
        except ValueError:
            print('Please choose a number between 0 and {0}'.format(len(disks)-1))
    return disks[choice]
    
def partition_disk(disk):
    parted_cmd = 'parted -s {0} -- mklabel gpt'.format(disk.name)
    run_cmd(parted_cmd, 'SHELL')
    makeparts_cmd = 'sgdisk --new=1:0:+550M --change-name=1:"EFI" --typecode=1:ef00 --largest-new=2 --change-name="MasterVolumeGroup" --typecode=2:8e00 {0}'.format(disk.name)
    run_cmd(makeparts_cmd, 'SHELL')

    n_disks = find_partitions()
    for nd in n_disks:
        if nd.name == disk.name:
            disk = nd
            break
    efi_part = None
    lvm_part = None
    for p in disk.partitions:
        if p.size <= 600000000: # EFI should be 550 MiB
            efi_part = p
        else:
            lvm_part = p
    format_efi_cmd = 'mkfs.vfat -F32 {0}'.format(efi_part.name)
    run_cmd(format_efi_cmd, 'SHELL')
    
    cryptsetup_cmds = [
        'cryptsetup luksFormat --type luks2 {0}'.format(lvm_part.name),
        'cryptsetup open {0} cryptlvm'.format(lvm_part.name),
        'pvcreate /dev/mapper/cryptlvm',
        'vgcreate MasterVolumeGroup /dev/mapper/cryptlvm',
        'lvcreate -L 40G MasterVolumeGroup -n root',
        'lvcreate -l 100%FREE MasterVolumeGroup -n home',
        'mkfs.ext4 /dev/MasterVolumeGroup/root',
        'mkfs.ext4 /dev/MasterVolumeGroup/home'
    ]

    for cmd in cryptsetup_cmds:
        run_cmd(cmd, 'SHELL')
    
    return {
        'diskname': disk.name,
        'boot': efi_part.name,
        'root': '/dev/MasterVolumeGroup/root',
        'home': '/dev/MasterVolumeGroup/home',
        'rootuuid': lvm_part.uuid
    }

def mount_target_parts(parts):
    cmds = [
        'mount {0} /mnt'.format(parts['root']),
        'mount {0} /mnt/boot'.format(parts['boot']),
        'mount {0} /mnt/home'.format(parts['home'])
    ]

    for i, cmd in enumerate(cmds):
        if i == 1:
            os.makedirs('/mnt/boot')
            os.makedirs('/mnt/home')
        run_cmd(cmd, 'SHELL')
